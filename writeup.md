# AES tricks

## Source

```python
#!/usr/bin/env python3
from hashlib import md5
from base64 import b64decode
from base64 import b64encode
import json
import string

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Util.Padding import pad, unpad


class AESCipher:
    def __init__(self, key, iv):
        self.key = key
        self.iv = iv

    def encrypt(self, data):
        self.cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return b64encode(self.cipher.encrypt(pad(data.encode('utf-8'), AES.block_size)))

    def decrypt(self, data):
        raw = b64decode(data)
        self.cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return unpad(self.cipher.decrypt(raw), AES.block_size).decode("utf-8", "ignore")



aes = AESCipher(get_random_bytes(AES.block_size), get_random_bytes(AES.block_size))

while True:
    print('''Chose option:
1. Encrypt your message
2. Talk to me (I accept only encrypted messages)''')
    option = input();

    if option == "1":
        print("Type message you want to encrypt: ")
        message = input()
        if message == "flag":
            print("Im sorry you cant encrypt this message")
            continue
        print("Your encrypted message: ", str(aes.encrypt(json.dumps({"m": message}))))


    if option == "2":
        print("Send me encrypted message: ")

        try:
            message = json.loads(aes.decrypt(input()))
            print("Your message: ", message)

            if message["m"] == "hi":
                print("Hello")

            if message["m"] == "ping":
                print("pong")

            if message["m"] == "flag":
                print("[REDACTED]")

        except Exception as e:
            print("There was an error while reading your message :(")
            continue
```

## Analysis

Above program lets the user do two things: encrypt a supplied message (after wrapping it in json) with a secret key, or decrypt a message with the same key, parse it as json and give user the flag if said json contains a field called "m" set to "flag". Creating such a message is not possible in a straightforward way, though.

The cipher used (AES-CBC) alone could suggest a bit flipping attack and this is further reinforced by "flipping" mentioned in challenge description.

## AES-CBC bit flipping 101

AES-CBC bit flipping attacks are possible because result of block decryption is xored with ciphertext from previous block. So, changing a bit (or more of them) in one block, will flip plaintext bit(s) on the same positions of the next block.

![AES-CBC decryption](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/CBC_decryption.svg/601px-CBC_decryption.svg.png "AES-CBC decryption")

![AES-CBC decryption](https://i.stack.imgur.com/bOu8Q.png "AES-CBC decryption")

Here's a simple example:

```
plaintext: "block0__________block1__________"
after encrypion
ciphertext0 hex: 1b12d0e0ccf70db04341ac750f45a492ab58e89d4615d2fb0ab10acd2681db5f
ciphertext1 hex: 3b12d0e0ecf70db04341ac750f45a492ab58e89d4615d2fb0ab10acd2681db5f
                 ^       ^
ciphertext1 has bytes 0 and 4 xored with 0x20

decrypted ciphertext1: "#g*\x86\xa1q+\x90+G\x9c\xf2tl\x93\x19BlocK1__________"
above xors resulted in changing 'b' and 'k' to uppercase     ^   ^
(equivalent with xoring with 0x20)
```

As can be seen in the example flipping bits in bytes 0 and 4 in block 0, flipped the same bits in the next block, but also completely scrambled the modified block.

## What now

The options of using CBC bitflipping may seem limited at first: not only modified block would have to result in a valid json, the whole input is also limited to the "m" field of the resulting json, the same one that has to be modified to get the flag.

## Json parsing

What would happen if one parsed json with multiple fields with the same name?

```python
>>> json.loads('{"m": 0, "m": 1}')
{'m': 1}
```

Seems like only one value is left in resulting object: the second one. This is great news - thanks to this behaviour, it should be possible to craft message in a way that could later allow to flip some bits in ciphertext so that second "m" field would appear.

```
# supply a message "_________________________','m': 'flag'"
| block0       || block1       || block2       |
{"m": "_________________________','m':'flag'}"}

# xor necessary bits in block1, so that "m":"flag" appears in block2
| block0       || block1       || block2       |
{"m": "_________< messed up    >","m":"flag"}__
# last two characters changed to spaces      ^^
```

In the above plan original `'` characters are changed (by modifying previous block of ciphertext) to `"`, and additional `"}` at the end is transformed to spaces. If message like above is parsed, first occurance of `"m"` should be ignored, and an object equivalent to `{"m": "flag"}` should come out.

## What about block 1?

There's an obvious problem with the above approach: block 1 (which can be assumed to be random looking after changing the necessary bits) has to first be decoded as utf8, and then parsed as valid contents of json string. Fortunetely first part is assured by the implementation already:

```python
def decrypt(self, data):
    raw = b64decode(data)
    self.cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
    return unpad(self.cipher.decrypt(raw), AES.block_size).decode("utf-8", "ignore")
```

`decode()` uses `"ignore"` as a method of dealing with invalid utf8, sequences, any offending bytes will just be removed.

The conastraint on json string are quite simple, characters below 0x20 have to be input as an escape sqeuence (e.g. to insert newline one has to use '\n' or \u000a) and `"` and `\` characters have to be escaped with backslash (`\"`, `\\`).

So out of all 256 byte values:

- 0x00-0x1f just won't work (json parser won't be happy),
- 0x20-0x7f are ok, with exception of `"` and `\` which will likely trigger json parser as well
- 0x80-0xff will likely be eaten by utf8 decoding (or form a valid utf8 character which are also allowed in json string) - none of these situations create any problems

Chance of success for single byte to not be an illegal json character: $\frac{256-34}{256}$ (34 is the number of bytes that will likely mess something up: bytes 0x00-0x1f, `"`, `\`).

Chance of success for the whole block to be a valid json string: $(\frac{256-34}{256})^{16} \approx 0.1$

So it might be necessary to try 10 or 20 times, but eventually this method should succeed. Below is a python solution (admittedly not very well automated):

```python
#!/usr/bin/env python3

from pwn import *

import base64

REMOTE = False

def xor_bytes(b0, b1):
    return bytes(a ^ b for a, b in zip(b0, b1))

def try_hack():
    if REMOTE:
        target = remote('chall.knping.pl', 22230)
    else:
        target = process('chall.py')

    target.sendline('1')
    line = target.recvuntil('Type message you want to encrypt:')
    print(line)
    # last string ("','m':'flag'}") should be starting at block 2
    target.sendline(' ' * (32 - len('{"m": "')) + "','m':'flag'}")

    print(target.recvline())
    line = target.recvline().decode()
    encrypted_b64 = re.match("[^']+b'([a-zA-Z0-9+/=]+)", line)[1]
    print(f'base64 message: {encrypted_b64}')
    encrypted = base64.b64decode(encrypted_b64)
    print(f'encrypted message: {encrypted}')

    # flip the following (in, the next block, hoping there won't be any illegal characters in this after change)
    # decoding is done with "ignore", so unicode errors are not a problem, and are removed
    # the only problem are characters < 0x20 and "'", the rest of chars are valid or would be removed
    # it can be assumed that each character is generated "randomly" after the bitflip
    # so the chance of having all chars in flipped block valid is about (1-(0x21/0x100))^16 which is about 0.1
    #                                 '   ,   '   m   '   :   '   f   l   a   g   '   }   "   }
    #                                 V       V       V       V                   V       V   V
    #                                 "   ,   "   m   "   :   "   f   l   a   g   "   }   _   _ (last two are spaces, not underscores)
    BITFLIP_STR = b'\x00' * 16 + b'\x05\x00\x05\x00\x05\x00\x05\x00\x00\x00\x00\x05\x00\x02\x5d\x00' + b'\x00'*16*8
    encrypted_flipped = xor_bytes(encrypted, BITFLIP_STR)

    target.sendline('2')
    line = target.recvuntil('Send me encrypted message:')
    print(line)
    target.sendline(base64.b64encode(encrypted_flipped))

    target.interactive()
    target.close()

for i in range(16):
    try_hack()

```

## Secrecy vs integrity

While it's not necessary for solving this problem, it's worth knowing why an attack like this is at all possible. It is because AES-CBC (and many other modes of operation) only guarantee data secrecy, which means that adversary shouldn't be able to recover original message from the ciphertext. It doesn't make any assurances about data integrity or authenticity - the server has no way of knowing if message it receives was created by itself, and if it was in any way modified. To implement something like above more securly, one might have to either add a [message authentication code](https://en.wikipedia.org/wiki/Message_authentication_code), or use an [authenticated encryption](https://en.wikipedia.org/wiki/Authenticated_encryption).
