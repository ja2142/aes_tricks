#!/usr/bin/env python3

from pwn import *

import base64

REMOTE = False

def xor_bytes(b0, b1):
    return bytes(a ^ b for a, b in zip(b0, b1))

def try_hack():
    if REMOTE:
        target = remote('chall.knping.pl', 22230)
    else:
        target = process('chall.py')

    target.sendline('1')
    line = target.recvuntil('Type message you want to encrypt:')
    print(line)
    # last string ("','m':'flag'}") should be starting at block 2
    target.sendline(' ' * (32 - len('{"m": "')) + "','m':'flag'}")

    print(target.recvline())
    line = target.recvline().decode()
    encrypted_b64 = re.match("[^']+b'([a-zA-Z0-9+/=]+)", line)[1]
    print(f'base64 message: {encrypted_b64}')
    encrypted = base64.b64decode(encrypted_b64)
    print(f'encrypted message: {encrypted}')

    # flip the following (in, the next block, hoping there won't be any illegal characters in this after change)
    # decoding is done with "ignore", so unicode errors are not a problem, and are removed
    # the only problem are characters < 0x20 and "'", the rest of chars are valid or would be removed
    # it can be assumed that each character is generated "randomly" after the bitflip
    # so the chance of having all chars in flipped block valid is about (1-(0x21/0x100))^16 which is about 0.1
    #                                 '   ,   '   m   '   :   '   f   l   a   g   '   }   "   }
    #                                 V       V       V       V                   V       V   V
    #                                 "   ,   "   m   "   :   "   f   l   a   g   "   }   _   _ (last two are spaces, not underscores)
    BITFLIP_STR = b'\x00' * 16 + b'\x05\x00\x05\x00\x05\x00\x05\x00\x00\x00\x00\x05\x00\x02\x5d\x00' + b'\x00'*16*8
    encrypted_flipped = xor_bytes(encrypted, BITFLIP_STR)

    target.sendline('2')
    line = target.recvuntil('Send me encrypted message:')
    print(line)
    target.sendline(base64.b64encode(encrypted_flipped))

    target.interactive()
    target.close()

for i in range(16):
    try_hack()
